import 'package:flutter/material.dart';
import 'shopClass.dart';
import 'localFiles/itemList.dart';
import 'Favorite.dart';
import 'package:flutter/services.dart';

//Widget responsible for the Pantry tab.
//Pantry will keep track of items that the user has bought, or already has at home.
//The idea is that it will help the user figure out and remember what they already
//have, and not buy any extra groceries they won't need.
//Functions similar to the Shopping list. The user will be able to add and remove
//items to the Pantry list.
class PantryTab extends StatefulWidget {
  @override
  PantryTabState createState() => PantryTabState();
}

class PantryTabState extends State<PantryTab> {
  @override
  Widget build(BuildContext context) {
    return PantryListView();
  }
}

class PantryListView extends StatefulWidget {
  @override
  _PantryListViewState createState() => _PantryListViewState();
}



class _PantryListViewState extends State<PantryListView>  {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color.fromRGBO(235, 235, 235, 1),
      //height: 350,
      child: Column(
        children: [
          Expanded(child:
            ListView.builder(
            //clipBehavior: Clip.none,
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                itemCount: pantryList.length,
                itemBuilder: (BuildContext context, int index) => pantryItem(index)
            ),
          ),
          
          Container(
            clipBehavior: Clip.none,
            alignment: Alignment.bottomRight,
            padding: EdgeInsets.all(8),
            child: FloatingActionButton(
              child: Icon(Icons.add),
              backgroundColor: Colors.blue,
              onPressed: () {
                print(pantryList.length);
                addItemDialog(context);
              }
            ),
          ),
        ],
      )
    );
  }

  @override
  Future addItemDialog(BuildContext context) async {

    String itemName;
    int quantity = 0;
    int stock = 0;
    foodGroup itemGroup;

    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Add Item"),
            content: StatefulBuilder(
              builder: (BuildContext context, StateSetter _setState) {
                return Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: new TextField(
                            autofocus: true,
                            decoration: InputDecoration(
                              labelText: 'item name',
                              hintText: 'apples, tea, etc.',
                            ),
                            onChanged: (value) {
                              _setState(() {
                                itemName = value;
                              });
                            },
                          )
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: DropdownButton<foodGroup>(
                            hint: Text('select item type'),
                            isExpanded: true,
                            value: itemGroup,
                            items: foodGroup.values.map((foodGroup foodGroupItem) {
                              return DropdownMenuItem<foodGroup>(
                                value: foodGroupItem,
                                child: Text(foodGroupItem.toString().replaceAll('foodGroup.', '')),
                              );
                            }).toList(),
                            onChanged: (foodGroup value) {
                              _setState(() {
                                itemGroup = value;
                              });
                            },
                            onTap: () {
                              FocusManager.instance.primaryFocus.unfocus();
                            },
                          )
                        ),
                      ],
                    ),
                        Row(
                          children: [
                            Expanded(
                                child: new TextField(
                                  decoration: InputDecoration(
                                    labelText: 'in pantry',
                                    hintText: '$quantity',
                                  ),
                                  keyboardType: TextInputType.number,
                                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],

                                  onChanged: (value) {
                                    _setState(() {
                                      quantity = num.tryParse(value);
                                    });
                                  },
                                )
                            ),
                            SizedBox(width: 60),
                            Expanded(
                                child: new TextField(
                                  decoration: InputDecoration(
                                    labelText: 'wanted',
                                    hintText: '$stock',
                                  ),
                                  keyboardType: TextInputType.number,
                                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],

                                  onChanged: (value) {
                                    _setState(() {
                                      stock = num.tryParse(value);
                                    });
                                  },
                                )
                            ),
                          ],
                        ),
                  ],
                );
              }
            ),
            actionsPadding: EdgeInsets.only(left: 10, right: 16),
            actions: [
              FlatButton(
                color: Colors.blue[400],
                child: const Text('Add',
                  style: TextStyle(
                      fontSize: 16,
                      color: Colors.white
                  ),
                ),
                onPressed: () {
                  if(itemName != null && itemGroup != null){
                    setState(() {
                      // add item to list
                      // display 'added item' snackbar
                      ListItem item = new ListItem.quantity(itemName, itemGroup, stock, quantity);
                      pantryList.insert(0, item);
                      buildList();
                    });
                    Navigator.of(context).pop();

                  }
                },
              ),
            ],
          );
        }
    );
  }

  //Contains the cards for each item found within the list pantryList
  Widget pantryItem(int index) {
    return Column(
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          height: 80,
          padding: EdgeInsets.all(5),
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(10),
                bottomLeft: Radius.circular(10),
              ),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: [
                Row(
                  children: [
                    IconButton(
                        icon: Icon(pantryList[index].favourite
                          ? Icons.favorite
                          : Icons.favorite_border),
                        color: pantryList[index].favourite
                            ? Colors.redAccent
                            : Colors.grey[400],
                    onPressed: () {
                          setState(() {
                            pantryList[index].favourite = !pantryList[index].favourite;
                            favoritesList();
                          });
                        }
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          height: 25,
                          width: 196,
                          child: FittedBox(
                            fit: BoxFit.scaleDown,
                            alignment: Alignment.centerLeft,
                            child: Text(
                              '${pantryList[index].itemName}',
                              style: TextStyle(fontSize: 20),
                            ),
                          ),
                        ),

                        Text(
                          foodGroupToString(pantryList[index].itemGroup),
                          style: TextStyle(color: Colors.black54),
                        ),
                      ],
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    IconButton(
                        iconSize: 16,
                        splashRadius: 16,
                        icon: Icon(
                          Icons.remove,
                          color: pantryList[index].itemQuantity > 0
                              ? Colors.grey[600]
                              : Colors.grey[400],),
                        onPressed: () {
                          setState(() {
                            if(pantryList[index].itemQuantity > 0)
                              pantryList[index].itemQuantity--;
                            buildList();
                          });
                        }
                    ),
                    Container(
                      alignment: Alignment.center,
                      width: 24,
                      child: Text(
                        (pantryList[index].itemQuantity).toString(),
                        style: TextStyle(
                          color: Colors.grey[800],
                        ),
                      ),
                    ),
                    IconButton(
                      iconSize: 16,
                      splashRadius: 16,
                      icon: Icon(
                        Icons.add,
                        color: Colors.grey[600],),
                      onPressed: () {
                        setState(() {
                          pantryList[index].itemQuantity++;
                          buildList();
                        });
                      }
                    ),
                    SizedBox(width: 10),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

}


/*

 */
/*
Divider(
                      thickness: 1,
                      indent: 10,
                      endIndent: 10,
                    ),
 */

/*

 */
/*
Divider(
                      thickness: 1,
                      indent: 10,
                      endIndent: 10,
                    ),
 */
