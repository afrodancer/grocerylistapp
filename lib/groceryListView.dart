import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'shopClass.dart';
import 'localFiles/itemList.dart';
import 'package:flutter/services.dart';
import 'dart:io';

//Widget responsible for the Shopping list tab.
//By default, user will be able to see all items on the list.
//**Want to make a FilterChip that allows the user to filter out items
//based on their food group (where they would be found in the grocery store).
//This would make finding everything on your list much faster.
//**Want to make Card(?) or ListTile(?) with trailing widgets (buttons) to allow
//user to "check off" items without removing them, or remove them from the list entirely.
//**Want to make function where all items that have been "checked off" can be added
//to the Pantry tab at once.
//**Want to make function where all items on the Shopping list can be removed
//entirely at once.
class ShopListTab extends StatefulWidget {
  @override
  _ShopListTabState createState() => _ShopListTabState();
}

class _ShopListTabState extends State<ShopListTab> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: buildList(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        List<Widget> widgetChilds;
        if (snapshot.hasError) {
          widgetChilds = errorWidget();
        } else {
          widgetChilds = loadingWidget();
        }

        return snapshot.hasData ? ShoppingListView() : displayWidgetChilds(widgetChilds);
      },
    );
  }

  List<Widget> loadingWidget () {
     return <Widget>[
      SizedBox(
        child: CircularProgressIndicator(),
        width: 60,
        height: 60,
      ),
      const Padding(
        padding: EdgeInsets.only(top: 16),
        child: Text('Loading...'),
      )
    ];
  }

  List<Widget> errorWidget() {
    return <Widget>[
      Text('An Error has occurred. Please reload the app.'),
    ];
  }

  Widget displayWidgetChilds(List<Widget> widgets) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: widgets,
      ),
    );
  }
}

class ShoppingListView extends StatefulWidget {
  @override
  _ShoppingListViewState createState() => _ShoppingListViewState();
}

class _ShoppingListViewState extends State<ShoppingListView> {
  ScrollController _scrollController = new ScrollController();
  bool _visible = true;
  bool _empty = false;

  @override
  void initState() {
    super.initState();
    checkList();
    handleScroll();
    cartList = [];
    for(int i = 0; i < shopList.length; i++) {pantryList[shopList[i]].purchaseState = 0;}
  }

  //These lines of code help animate the purchase button for easy readability when viewing shopping list
  @override
  void dispose() {
    _scrollController.removeListener(() {});
    super.dispose();
  }

  void showFloatingButton() {
    setState(() {
      _visible = true;
    });
  }

  void hideFloatingButton() {
    setState(() {
      _visible = false;
    });
  }

  void handleScroll() async {
    _scrollController.addListener(() {
      if (_scrollController.position.userScrollDirection ==
          ScrollDirection.reverse) hideFloatingButton();
      if (_scrollController.position.userScrollDirection ==
          ScrollDirection.forward) showFloatingButton();
    });
  }
  // ********************* END OF PURCHASE ANIMATOR CONTROLLER *********************

  //This function hides the cart icon if there are no items within the shopping list
  void checkList() {
    if (shopList.length == 0) {
      _empty = true;
      _visible = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(235, 235, 235, 1),
      body: _empty ? emptyShoppingListDisplay() : shoppingListDisplay(),
      floatingActionButton: AnimatedOpacity(
        opacity: _visible ? 1.0 : 0.0,
        duration: Duration(milliseconds: 100),
        child: FloatingActionButton(
          child: Icon(Icons.shopping_basket),
          backgroundColor: Colors.green,
          onPressed: () {
            if (cartList.length == 0) {
              final showSnackBar = SnackBar(
                content: Text('There are no items in your cart!'),
                action: SnackBarAction(
                  label: 'Dismiss',
                  onPressed: () {},
                ),
                duration: const Duration(seconds: 2),
                backgroundColor: Colors.black,
                behavior: SnackBarBehavior.floating,
              );

              Scaffold.of(context).showSnackBar(showSnackBar);
            } else {
              cartDisplay();
            }
          },
        ),
      ),
    );
  }

  //Contains the cards for each item found within the list shopList
  Widget shopItem(int index) {
    //bool _selected = false;
    return Column(
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          height: 80,
          padding: EdgeInsets.all(5),
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(10),
                bottomLeft: Radius.circular(10),
              ),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    SizedBox(width: 10),
                    Container(
                      child: GestureDetector(
                        child: determineIcon(pantryList[shopList[index]].purchaseState),
                        onTap: () {
                          setState(() {
                            if (pantryList[shopList[index]].purchaseState == 1 || pantryList[shopList[index]].purchaseState == 0) {

                              pantryList[shopList[index]].purchaseState = 2;

                              if (!cartList.contains(shopList[index]))
                                cartList.add(shopList[index]);

                            } else {
                              pantryList[shopList[index]].purchaseState = 0;
                              cartList.remove(shopList[index]);
                            }

                          });
                        },
                        onLongPress: () {
                          setState(() {
                            pantryList[shopList[index]].purchaseState = 1;

                            if (!cartList.contains(shopList[index]))
                              cartList.add(shopList[index]);
                          });
                        },
                      ),
                    ),
                    SizedBox(width: 10),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: 200,
                          height: 25,
                          child: FittedBox(
                            fit: BoxFit.scaleDown,
                            alignment: Alignment.centerLeft,
                            child: Text(
                              '${pantryList[shopList[index]].itemName}',
                              style: TextStyle(fontSize: 20),
                            ),
                          ),
                        ),
                        Text(
                          foodGroupToString(pantryList[shopList[index]].itemGroup),
                          style: TextStyle(color: Colors.black54),
                        ),
                      ],
                    )
                  ],
                ),
                Row(
                  children: [
                    Container(
                      height: 40,
                      width: 100,
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(0),
                            bottomLeft: Radius.circular(0),
                          ),
                        ),
                        elevation: 0,
                        color: Colors.grey.shade200,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            SizedBox(width: 2),
                            Text('Quant. '),
                            //VerticalDivider(width: 10, thickness: 1, color: Colors.black54,),
                            GestureDetector(
                              child: Container(
                                height: 40,
                                width: 35,
                                decoration: BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.shade400,
                                    ),
                                    BoxShadow(
                                      color: Colors.grey.shade100,
                                      spreadRadius: -1,
                                      blurRadius: 12.0,
                                    ),
                                  ],
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                        ((pantryList[shopList[index]].itemStock + pantryList[shopList[index]].needed) - pantryList[shopList[index]].itemQuantity).toString(),
                                        textAlign: TextAlign.center),
                                  ],
                                ),
                              ),
                              onLongPress: () {
                                showDialog(
                                    context: context,
                                    builder: (BuildContext context){
                                      return Center(
                                        child: Container(
                                          child: Card(
                                            child: Column(
                                                children: []
                                            ),
                                          ),
                                        ),
                                      );
                                    });
                                },
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(width: 10),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  //Shows the icon displayed next to items depending if partial or full purchase
  Icon determineIcon(int val) {
    Icon checkBox;

    switch (val) {
      case 0:
        checkBox = Icon(Icons.check_box_outline_blank, color: Colors.redAccent);
        break;
      case 1:
        checkBox =
            Icon(Icons.indeterminate_check_box, color: Colors.deepOrangeAccent);
        break;
      case 2:
        checkBox = Icon(Icons.check_box, color: Colors.green);
        break;
    }

    return checkBox;
  }

  //Shows the items selected to be purchased using dialogs
  void cartDisplay() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Center(
            child: Container(
              height: MediaQuery.of(context).size.height - 150,
              width: MediaQuery.of(context).size.width - 50,
              child: Card(
                child: Column(
                  children: [
                    SizedBox(height: 10),
                    Text(
                      'Shopping Cart',
                      style: TextStyle(fontSize: 25),
                    ),
                    SizedBox(height: 20),
                    SizedBox(
                      height:
                      MediaQuery.of(context).size.height - 275,
                      child: ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.vertical,
                        itemCount: cartList.length,
                        itemBuilder:
                            (BuildContext context, int index) =>
                            Container(
                              child: Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: [
                                  SizedBox(width: 10),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        width: 200,
                                        height: 25,
                                        child: FittedBox(
                                          fit: BoxFit.scaleDown,
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            '${pantryList[cartList[index]].itemName}',
                                            style:
                                            TextStyle(fontSize: 20),
                                          ),
                                        ),
                                      ),
                                      Text(
                                        foodGroupToString(pantryList[cartList[index]].itemGroup),
                                        style: TextStyle(color: Colors.black54),
                                      ),
                                      SizedBox(height: 10),
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Container(
                                        height: 40,
                                        width: 100,
                                        child: Card(
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                            BorderRadius.only(
                                              topLeft: Radius.circular(0),
                                              bottomLeft:
                                              Radius.circular(0),
                                            ),
                                          ),
                                          elevation: 0,
                                          color: Colors.grey.shade200,
                                          child: Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment
                                                .spaceBetween,
                                            children: [
                                              SizedBox(width: 2),
                                              Text('Quant. '),
                                              //VerticalDivider(width: 10, thickness: 1, color: Colors.black54,),
                                              Container(
                                                height: 40,
                                                width: 35,
                                                decoration: BoxDecoration(
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: Colors
                                                          .grey.shade400,
                                                    ),
                                                    BoxShadow(
                                                      color: Colors
                                                          .grey.shade100,
                                                      spreadRadius: -1,
                                                      blurRadius: 12.0,
                                                    ),
                                                  ],
                                                ),
                                                child: Column(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                                  children: [
                                                    SizedBox(
                                                      height: 25,
                                                      width: 35,
                                                      child: TextFormField(
                                                        style: TextStyle(fontSize: 15),
                                                        textAlign: TextAlign.center,
                                                        cursorColor: Colors.transparent,
                                                        decoration: new InputDecoration(
                                                            border: InputBorder.none,
                                                            focusedBorder: InputBorder.none,
                                                            enabledBorder: InputBorder.none,
                                                            errorBorder: InputBorder.none,
                                                            disabledBorder: InputBorder.none,
                                                        ),
                                                        keyboardType: TextInputType.number,
                                                        initialValue: pantryList[cartList[index]].purchaseQuantity.toString(),
                                                        inputFormatters: [FilteringTextInputFormatter.digitsOnly],

                                                        onChanged: (value) {
                                                          setState(() {
                                                            pantryList[cartList[index]].purchaseQuantity = num.tryParse(value);
                                                          });
                                                        },
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      SizedBox(width: 10),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                      ),
                    ),
                    Align(
                      alignment: FractionalOffset.bottomCenter,
                      child: RaisedButton(
                        //color: Colors.blueAccent,
                        child: Text('Purchase?'),
                        onPressed: () {
                          setState(() {
                            int remain;

                            for (int y = 0; y < cartList.length; y++) {
                              pantryList[cartList[y]].itemQuantity += pantryList[cartList[y]].purchaseQuantity;

                              remain = pantryList[cartList[y]].purchaseQuantity - pantryList[cartList[y]].itemStock;

                              if(pantryList[cartList[y]].needed > 0) {
                                pantryList[cartList[y]].needed -= remain;

                                if(pantryList[cartList[y]].needed < 0)
                                  {
                                    pantryList[cartList[y]].needed = 0;
                                  }
                              }
                              pantryList[cartList[y]].purchaseState = 0;

                            }

                            buildList();
                            cartList = [];
                            Navigator.pop(context);
                          });
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  //Page display if there are NO items within the shopping cart
  Widget emptyShoppingListDisplay() {
    return Center(
      child: Container(
        //color: Color.fromRGBO(235, 235, 235, 1),
        //height: 350,
          child: Text('Your Shopping List is Empty!')),
    );
  }

  //Page display if there are items within the shopping cart
  Widget shoppingListDisplay() {
    return Center(
      child: Container(
        //height: 350,
        child: Column(
          children: [
            Expanded(
              child: ListView.builder(
                  shrinkWrap: true,
                  controller: _scrollController,
                  scrollDirection: Axis.vertical,
                  itemCount: shopList.length,
                  itemBuilder: (BuildContext context, int index) =>
                      shopItem(index)),
            ),
          ],
        ),
      ),
    );
  }
}
