import 'package:flutter/material.dart';
import 'localFiles/itemList.dart';

//Categories of food based on where you'd find it in the grocery store.
//**want to use a FilterChip to filter items on the list, make it easier to
//get all the similar items in one aisle
enum foodGroup {
  dairy,
  vegetables,
  pasta,
  fruit,
  meat,
  seafood,
  pastry,
  bread, //Apparently there is a difference
  beverage,
}

//Converts the foodGroup enumerators to a string without the "foodGroup." part
String foodGroupToString(foodGroup group) {
  String str = group.toString();
  str = str.replaceAll('foodGroup.', '');
  return str;
}

//Class used in lists that contains the information put in by the user.
//**Want to allow user to add items to the list by filling in the values,
// perhaps by an AlertDialog(?).
class ListItem {
  String itemName;
  foodGroup itemGroup;
  int itemQuantity;
  int itemStock;
  int purchaseQuantity;
  bool favourite;
  int purchaseState;
  int needed;
  ListItem(String name, foodGroup group){
    itemName = name;
    itemGroup = group;
    itemQuantity = 0;
    itemStock = 0;
    purchaseQuantity = 0;
    favourite = false;
    purchaseState = 2;
    needed = 0;
  }

  ListItem.quantity(String name, foodGroup group, int stock, int quan){
    itemName = name;
    itemGroup = group;
    itemStock = stock;
    itemQuantity = quan;
    purchaseQuantity = 0;
    favourite = false;
    purchaseState = 0;
    needed = 0;
  }
}

/*
Builds the shopping list present on the Shopping List tab, whenever there are changes
to the pantry list, this function can be called autoupdating the list.
 */
Future<bool> buildList() async {
  //shopList = [];

  for (int i = 0; i < pantryList.length; i++) {
    //print("P1 " + i.toString() + ": " + pantryList[i].itemName + " - " + pantryList[i].purchaseQuantity.toString());

    if (pantryList[i].itemQuantity < (pantryList[i].itemStock + pantryList[i].needed) && !shopList.contains(i)) {
      print(i.toString() + ": " + pantryList[i].itemName + " - " + pantryList[i].purchaseQuantity.toString());

      pantryList[i].purchaseState = 0;
      pantryList[i].purchaseQuantity = (pantryList[i].itemStock + pantryList[i].needed) - pantryList[i].itemQuantity;

      shopList.add(i);
    }
    else if(pantryList[i].itemQuantity >= pantryList[i].itemStock + pantryList[i].needed && shopList.contains(i)){
      shopList.remove(i);
    }

    //If the item is currently in the list but the number of items in stock has changed
    if(pantryList[i].itemQuantity < (pantryList[i].itemStock + pantryList[i].needed) && shopList.contains(i))
      {
        pantryList[i].purchaseQuantity = (pantryList[i].itemStock + pantryList[i].needed) - pantryList[i].itemQuantity;
      }

    if(favoriteList.contains(i) && shopList.contains(i))
      {
        shopList.remove(i);
        shopList.insert(0, i);
      }
  }
  //print('\n');

  return true;
}


