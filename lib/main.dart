import 'package:flutter/material.dart';
import 'groceryListView.dart';
import 'pantryListView.dart';
import 'Favorite.dart';

//main section of app that sets up all the tabs
void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  //This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            bottom: TabBar(
              tabs: [
                Tab(text: 'Shopping List'),
                Tab(text: 'Pantry'),
                Tab(text: 'Favorites')
              ]
            ),
          ),
          body: TabBarView(
            children: [
              ShopListTab(),
              PantryTab(),
              Favorite(),
              //FavoritesTab()
            ],
          ),
        ),
      )
    );
  }
}