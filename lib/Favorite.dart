import 'package:flutter/material.dart';
import 'shopClass.dart';
import 'pantryListView.dart';
import 'localFiles/itemList.dart';

//**** Bug deals with the quantity in the dialogue box not always being the quantity added to the shopping list.
// in some cases adding 2 will not add to the shopping list if initial quantity was 2, we would need to add more than that in the dialogue
//to get the item added to the actual shopping list. Not a hard fix mostly deals with the way that these variables are modified in other parts of the
//code. In the interest of time ive decided to leave the bug and begin the slides as looking back at all the code may take longer
//than i have.****


//Widget responsible for the Favorites tab.
//Favorites will keep track of items that the user frequently buys. Instead of
//having to type in the same items every time they put a shopping list together,
//the user can add items from the Favorites to the Shopping list.
//**Want to add a function where the user can add one, multiple, or all of the
//items on the Favorite Tab to the Shopping list.
class Favorite extends StatefulWidget {
  @override
  FavoriteState createState() => FavoriteState();
}

class FavoriteState extends State<Favorite> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height * 2,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: List.generate(favoriteList.length, (index) {
              return Container(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  height: 60,
                  width: MediaQuery.of(context).size.width,
                  child: Card(
                    color: Colors.grey[200],
                    shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(20),
                      bottomLeft: Radius.circular(20),
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(pantryList[favoriteList[index]].itemName,
                      textAlign: TextAlign.justify,
                      style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold,),),
                      IconButton(icon: Icon(Icons.add, color: Colors.lightGreen,),
                          onPressed: () {
                            return _showMyDialog(index);
                          }
                      ),
            ],
                  ),
              ),
            );
          }),
            ),
              ),
            );}

  Widget _buildQuantityContainer(int index) => Container(
  alignment: Alignment.center,
  height: 30,
  width: 20,
  color: Colors.black,
  child: Text(
  '${pantryList[favoriteList[index]].needed.toString()}',
  style: TextStyle(
  color: Colors.white,
  fontSize: 14),),
  );

Future<void> _showMyDialog(int ind) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return  AlertDialog(
      title: Text(
      'Would you like to add this item to your shopping list?'),
      content: SingleChildScrollView(
      child: ListBody(
      children: <Widget>[
      Text(
      'How many would you like to add? '),
      Text(
      'Use the + and - button to obtain the correct amount you wish to purchase later.'),
      Text('Note: Already have ${pantryList[favoriteList[ind]].itemStock.toString()} in the shopping list. \n'
            '(if adding an unlisted item to the shopping list, you will have to add more than ${pantryList[favoriteList[ind]].purchaseQuantity.toString()} for it to appear)',
      style: TextStyle(fontSize: 8),),
      _buildQuantityContainer(ind),
  ],
  ),
  ),
  actions: <Widget>[
  Container(
  height: 40,
  width: 120,
  child: Row(
  children: [
  IconButton(icon: Icon(Icons.add,
  color: Colors.lightGreen,),
  onPressed: () {
  setState(() {
  pantryList[favoriteList[ind]].needed += 1;
  Navigator.of(context).pop();
  _showMyDialog(ind);
  });
  }),
  IconButton(icon: Icon(
  Icons.remove,
  color: Colors.red,),
  onPressed: () {
  setState(() {
  if (pantryList[favoriteList[ind]].needed > 0)
  pantryList[favoriteList[ind]].needed -= 1;
  Navigator.of(context).pop();
  _showMyDialog(ind);
  });
  }),
  ],
  )
  ),
    RaisedButton(
      child: Text('Add to List'),
      onPressed: () {
        bool existing = false;
        ListItem temp = pantryList[favoriteList[ind]];
        for(int i = 0; i < pantryList.length; i++)
        {
          print(i.toString() + " needed: " + pantryList[favoriteList[ind]].needed.toString());
          print("purchaseQuantity: " + pantryList[favoriteList[ind]].purchaseQuantity.toString());
          if(pantryList[i].itemName == temp.itemName)
            {
              pantryList[i].purchaseQuantity = pantryList[favoriteList[ind]].needed;
              //pantryList[i].needed = 0;
              existing = true;
              break;
            }
        }
        if(!existing) {
          temp.purchaseQuantity = temp.needed;
          pantryList.add(temp);
        }
        //buildList();
        Navigator.of(context).pop();
        buildList();
      },
    ),
  RaisedButton(
  child: Text('Cancel'),
  onPressed: () {
  Navigator.of(context).pop();
  },
  ),
  ],
  );
  }
  );
}
}


Future<void>  favoritesList() async {
  for(int i = 0;i<pantryList.length; i++)
    {
      if(pantryList[i].favourite && !favoriteList.contains(i))
      {
        favoriteList.add(i);
      }
      else if(!pantryList[i].favourite && favoriteList.contains(i)) {
        pantryList[i].needed = 0;
        favoriteList.remove(i);
      }

      /*else if(!pantryList[i].favourite && pantryList[i].favListed)
        {
          bool inFav = false;
          int index = 0;
          while(!inFav)
            {
              if(index > pantryList.length)
                return;

              if(favoriteList[index].itemName == pantryList[i].itemName)
                inFav = true;

              index++;
            }
          favoriteList.removeAt(index - 1);
        }*/
    }

  buildList();
}