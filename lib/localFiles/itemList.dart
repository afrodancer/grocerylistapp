import '../shopClass.dart';
import 'package:flutter/material.dart';

List<ListItem> pantryList = [
  new ListItem.quantity('milk', foodGroup.dairy, 2, 0),
  new ListItem.quantity('broccoli', foodGroup.vegetables, 12, 0),
  new ListItem.quantity('spaghetti noodles', foodGroup.pasta, 5, 0),
  new ListItem.quantity('Turkey', foodGroup.meat, 1, 0),
  new ListItem.quantity('Loafs of Bread', foodGroup.bread, 5, 0),
  new ListItem.quantity('Sack of Sweet Potato', foodGroup.vegetables, 2, 0),
  new ListItem.quantity('Coca Cola', foodGroup.beverage, 2, 0),
  new ListItem.quantity('Pack Green Beans', foodGroup.vegetables, 4, 0),
  new ListItem.quantity('Canned Cranberry ', foodGroup.fruit, 2, 0),
];

List<int> shopList = [

];

List<int> cartList = [

];

List<int> favoriteList = [

];

